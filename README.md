# README #
 
Leafsubtract is an ImageJ macro script which opens a time-series of images and subtracts each image from the previous one to show differences between the two frames. Features include auto-cropping and auto-alignment based on user-specified regions of interest, and guided image/stack analysis.

Version 160804 - current functionality:

SUBTRACTION:

* imports image files (jpg, tif) from a source directory
* auto-crops to user-defined area
* auto-aligns images based on user-defined region of interest
* subtracts each image from the previous one 
* saves subtracted images to the output directory
* reduces image dimensions for smaller output (optional)
* creates an avi clip of subtracted images.
* saves a log file containing used settings and crop/align regions.

IMAGE ANALYSIS:

* imports subtraction tifs from source directory
* auto-creates results folder as subdirectory
* guides through analysis (stack smoothing, thresholding, manual slice corrections, definition of leaf area and leaf vein orders, exporting output to results folder)

R ANALYSIS - Leaf Vulnerability Curve:

* read output from ImageJ Macro Image Analysis
* read PSY-1 Stem Psychrometer native csv file
* correct PSY-1 readings using manual spot measurements


### Installation ###

To use the ImageJ macro:

* Download and install [ImageJ](http://rsb.info.nih.gov/ij/download.html) or [FIJI](http://fiji.sc/Downloads).
* Install the [Template Matching and Slice Alignment ImageJ plugin](https://sites.google.com/site/qingzongtseng/template-matching-ij-plugin#install) by Qingzong Tseng.
* Download the macro from [here](https://bitbucket.org/ponycopter/leafsubtract/downloads) or copy its source to a new text file. 
* Run the macro via Plugins > Macros > Run

To use the macro through a button in the ImageJ toolbar:

* Open the ImageJ installation directory and navigate to "macros".
* Open StartupMacros.txt (may be called StartupMacros.ijm, StartupMacros.fiji.ijm) with a text editor or in ImageJ.
* Copy the entire leafsubtract script and paste it at the end of the StartupMacros file. Remove the "//" from the very beginning of the third and last lines in the script. Save file.
* (Re-)Start ImageJ.
* Start the macro by clicking on the new two-square symbol in the toolbar ImageJ.

To use the R script: 

* Download the R script from [here](https://bitbucket.org/ponycopter/leafsubtract/downloads)
* Rename PSY-1 Stem Psychrometer output to "stempsy.csv" and place in results directory (optional)
* Adjust the setwd() command (line 1) to point to the results directory
* Run the script


### Who do I talk to? ###
* If you'd like to get in touch, find my current email address in line 2 of the macro, or over on my [personal website](http://www.thinkoholic.com).


### Credit ###
* Qinzong Tseng for sharing his template matching and slice alignment plugin.
* Samuel Audit, for the underlying [javac library](https://github.com/bytedeco).
* Richard Flavel (UNE), for introducing me to advanced ImageJ macros.