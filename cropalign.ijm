//====== Crop and align a sequence of images
//====== Script by Markus Nolf, m.nolf@westernsydney.edu.au, https://bitbucket.org/ponycopter/leafsubtract
//macro "MNO Image CropAlign Action Tool - C090L111bL11b1Lb1bbL1bbbC955L444eL44e4Le4eeL4eee"{
//image alignment requires this plugin: https://sites.google.com/site/qingzongtseng/template-matching-ij-plugin

	dowhat = "Subtract images";

	dir = getDirectory("Choose Source Directory");	
	list = getFileList(dir);
	nfiles = list.length;
	
	dir2 = getDirectory("Choose Output directory");
	
	Dialog.create("Please confirm...");
	Dialog.addMessage(nfiles+" files found.");
	Dialog.addCheckbox("auto-crop?",true);
	Dialog.addCheckbox("auto-align?",true);
	Dialog.addCheckbox("8-bit output?",false);
	Dialog.show();

	docrop = Dialog.getCheckbox();
	doalign = Dialog.getCheckbox();
	doeightbit = Dialog.getCheckbox();		

	//clean up: close all windows	
	print("...");
	run("Close All");
	IJ.deleteRows(0, 99999);
	selectWindow("Log");
	run("Close");
	setResult(0,0,1);
	selectWindow("Results");
	run("Close");

	//set timestamp for output of log files
	getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
	function filler(j) {
		if(j<10) { string="0" + toString(j,0); } else { string="" + toString(j,0); }
		return string;
	}
	tmpyear = substring(year,2,4); tmpmonth = filler(month+1);tmpday = filler(dayOfMonth);tmphour = filler(hour);tmpminute = filler(minute);tmpsecond = filler(second);
	tmptimestamp = tmpyear+tmpmonth+tmpday+"-"+tmphour+tmpminute+tmpsecond;

	z=round(nfiles/2);
	templatefile = list[z];
	while(!endsWith(templatefile,".jpg") & !endsWith(templatefile,".tif") & !endsWith(templatefile,".png")) {
		z=z+1;
		templatefile = list[z];
	}

	print("input dir: "+dir);
	print("output dir: "+dir2);

	print("first image: "+list[0]);
	print("last image: "+list[(nfiles-1)]);
	print("number of files: "+nfiles);
	print("\n");	
		
	print("auto-crop: "+docrop+"\n");
	if(docrop==1) {
		open(dir+File.separator+templatefile);
		setTool("rectangle");
		waitForUser("Select CROP area","Please select region of interest for cropping.");

		roiType = selectionType();
		getSelectionBounds(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);

		//fix for: when user selects full width in templatefile, but image dimensions in next images may be smaller dimension (scanner problem)
		ImageWidth = getWidth(); 
		ImageHeight = getHeight();
		if(cropSelectionX<=10) {
			cropSelectionX = 10;
			print("adjusting ROI:x to 10");
		}
		if(cropSelectionX+cropSelectionWidth>=ImageWidth-10) {
			cropSelectionWidth=ImageWidth-cropSelectionX-10;
			print("adjusting ROI:width to image-width minus 10");
		}
		if(cropSelectionY<=10) {
			cropSelectionY = 10;
			print("adjusting ROI:y to 10");
		}
		if(cropSelectionY+cropSelectionHeight>=ImageHeight-10) {
			cropSelectionHeight=ImageHeight-cropSelectionY-10;
			print("adjusting ROI:height to image-height minus 10");
		}
		makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
		run("Crop");
	}
	if(docrop!=1) {
		open(dir+File.separator+templatefile);
		cropSelectionX = 0;
		cropSelectionY = 0;
		cropSelectionWidth=getWidth();
		cropSelectionHeight=getHeight();
		run("Close");		
	}
	if(doalign==1) {
		if(docrop!=1) {		
			open(dir+File.separator+templatefile);
		}
		autoalignx1 = round(cropSelectionWidth/2-cropSelectionWidth/5);
		autoaligny1 = round(cropSelectionHeight/2-cropSelectionHeight/5);
		autoalignw = round(cropSelectionWidth/5*2);
		autoalignh = round(cropSelectionHeight/5*2);
		makeRectangle(autoalignx1, autoaligny1, autoalignw, autoalignh);
		setTool("rectangle");
		waitForUser("Select ROI","Please select region of interest for image alignment.");

		roiType = selectionType();
		getSelectionBounds(roiSelectionX, roiSelectionY, roiSelectionWidth, roiSelectionHeight);

		//if roiSelection is biger than cropSelection, auto-align will fail. in this case, reduce roi boundary here: 
		if(roiSelectionWidth-cropSelectionWidth>20) { roiSelectionWidth = cropSelectionWidth-20; }
		if(roiSelectionHeight-cropSelectionHeight>20) { roiSelectionHeight = cropSelectionHeight-20; }

		run("Set Measurements...", "decimal=9");
	}

	print("   x: "+cropSelectionX+"\n   width: "+cropSelectionWidth+"\n   y: "+cropSelectionY+"\n   height: "+cropSelectionHeight+"\n ");
	print("auto-align: "+doalign);
	if(doalign==1) {
		print("   x: "+roiSelectionX+"\n   width: "+roiSelectionWidth+"\n   y: "+roiSelectionY+"\n   height: "+roiSelectionHeight+"\n ");
	}
	if(docrop+doalign>0) {	run("Close"); }
	print("\n");

	selectWindow("Log");  
	logfilename=dir2+"00-log-"+tmptimestamp+".txt";
	saveAs("Text", logfilename); 	
	
	setBatchMode(true);
	if (docrop+doalign>0) {
		setBatchMode(true);
		print("\n Processing images now. \n");
		for (i=0; i<(nfiles-1); i++) {
			showProgress(i+1, nfiles);
			j=i+1;
			firstfile = list[i];
			secondfile = list[j];
	
			if((endsWith(list[i],".jpg") & endsWith(list[j],".jpg")) | (endsWith(list[i],".tif") & endsWith(list[j],".tif")) | (endsWith(list[i],".png") & endsWith(list[j],".png"))) {
				print(j+" / "+(nfiles-1)+": processing "+secondfile+" and "+firstfile);

				open(dir+templatefile);
				if(docrop==1) {
					makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
					run("Crop");
				}				
				
				open(dir+firstfile);
				filename1 = replace(firstfile,".jpg","");
				filename1 = replace(filename1,".tif","");
				filename1 = replace(filename1,".png","");
				rename(filename1);
				if(docrop==1) {
					makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
					run("Crop");
				}
			
				open(dir+secondfile);
				filename2 = replace(secondfile,".jpg","");
				filename2 = replace(filename2,".tif","");
				filename2 = replace(filename2,".png","");
				rename(filename2);
				if(docrop==1) {
					makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
					print("  cropping...");
					run("Crop");
				}			

				if(doalign==1){
					run("Images to Stack", "method=[Copy (center)] name=Stack title=[] use");
					run("8-bit"); 
					print("  aligning slices...");
					setBackgroundColor(122,122,122);
					run("Align slices in stack...", "method=5 windowsizex="+roiSelectionWidth+" windowsizey="+roiSelectionHeight+" x0="+roiSelectionX+" y0="+ roiSelectionY+" swindow=20 subpixel=true itpmethod=1 ref.slice=1 show=TRUE");
					
					xshift1 = getResult("dX",0); 
					yshift1 = getResult("dY",0); 
					xshift2 = getResult("dX",1); 
					yshift2 = getResult("dY",1); 
					selectWindow("Stack");
					run("Close");
	
				}
	
				open(dir+firstfile);
				rename(filename1);
				if(doalign==1) { 
					setBackgroundColor(122, 122, 122);
					run("Translate...", "x="+xshift1+" y="+yshift1+" interpolation=Bilinear");
				}
				makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
				run("Crop");
	
				open(dir+secondfile);
				rename(filename2);
				if(doalign==1) { 
					setBackgroundColor(122, 122, 122);
					run("Translate...", "x="+xshift2+" y="+yshift2+" interpolation=Bilinear");
				}
				makeRectangle(cropSelectionX, cropSelectionY, cropSelectionWidth, cropSelectionHeight);
				run("Crop");
	
				windowchooser = filename2;
				selectWindow(windowchooser);
				if(docrop==1 && doalign!=1) { outputname = filename2+"-crop.jpg"; }
				if(docrop!=1 && doalign==1) { outputname = filename2+"-align.jpg"; }
				if(docrop==1 && doalign==1) { outputname = filename2+"-crop-align.jpg"; }

				
				rename(outputname);
				finalname=dir2+outputname;
	
				if(doeightbit==1) {
					run("8-bit");
				}
				saveAs("Tiff", finalname);
				run("Close All");
			} else { // end of if endswith .jpg
				print(j+": skipping "+secondfile+" - "+firstfile);
			}
		}
		
		//clean up: close all windows (open them first if they're not open) 
		open(dir+templatefile);
		run("Measure");		
		run("Close");
		
		selectWindow("Results");
		run("Close");
		setBatchMode(false);
		
		selectWindow("Log");
		logfilename=dir2+"00-log-"+tmptimestamp+".txt";
		saveAs("Text", logfilename); 
	} //end if(docrop+doalign>0)
	
	setBatchMode(false);

print("\n All done.");


//} //end of macro
