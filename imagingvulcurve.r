#setwd('C:\\Leaf Images\\output EUCobl-1-Microscope\\results')
#setwd('C:\\Users\\markus\\Dropbox\\uni\\leafimaging-curvefitting\\test')
setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\test')
#setwd('C:\\Users\\markus\\Dropbox\\uni\\leafimaging-curvefitting\\')

setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output ACAmel-4top--32bit-deflicker-crop-new-results')
setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output ACAmel-5microscope-4--32bit-deflicker-results')
setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output ACAmel-5microscope-4--32bit-deflicker-results2') 
setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output EUCsoc 101 - Microscope-results') 
setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output EUCsoc 102&3 - Scanner bottom new-results') 
#setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output ACAmel-4top--32bit-deflicker-crop-results') #crap dataset!
#setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output EUCsoc 102&3 - Scanner top-results') #crap dataset
#setwd('C:\\Users\\30035327\\Dropbox\\uni\\leafimaging-curvefitting\\output NOTcun-RD1bottom-Scanner-results') #crap dataset




#todo: 
# use manual WP measurements only
# mixed effects fit? pool multiple runs
#DONE output file: time, cumul. area, wp
# append maunal WP measurements beyond end of sensible stem psy data

#Sys.setenv(R_LIBS = "c:\\r-scripts\r-library\\")
#.libPaths()

library(fitplc)

output = 1
#output = 1


#####################################################################################################
# prepare imaging data for processing
#####################################################################################################
tmpinfo <- unlist(strsplit(getwd(),"/"))
samplename <- gsub("output ","",tmpinfo[length(tmpinfo)-1])
#samplename <- scan('sample.txt',"character", sep=";")

slicelist <- scan('slicenames.txt',"character", sep=";")
slicelist <- slicelist[slicelist!=""]
head(slicelist)
slicetimestamp <- data.frame(list(string=as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 3))),datetime=as.POSIXct(as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 3))), format="%m%d%H%M%S")))

if(is.na(slicetimestamp$datetime[1])) {
  slicetimestamp <- data.frame(list(string=
                                      paste(as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 3))), as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 4))))
                                    ,datetime=as.POSIXct(
                                      paste(as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 3))), as.vector(unlist(lapply(strsplit(slicelist,"-"),'[[', 4))))
                                      , format="%Y%m%d%H%M%S")))
}

tail(slicetimestamp)

allcombined <- read.table("output-0entireleaf.txt", sep="\t", header=TRUE)
head(allcombined)
firstorder <- read.table("output-1storder.txt", sep="\t", header=TRUE)
head(firstorder)
secondorder <- read.table("output-2ndorder.txt", sep="\t", header=TRUE)
head(secondorder)
thirdorder <- read.table("output-3rdorder.txt", sep="\t", header=TRUE)
head(thirdorder)


#####################################################################################################
# prepare stem-psy data for processing
#####################################################################################################

if(exists("stempsy") { remove(stempsy); }
stempsy <- read.csv("stempsy.csv",sep=",", skip=15, stringsAsFactors=FALSE)
if(exists("stempsy")) {
  stempsy$Datetime <- as.POSIXct(paste(stempsy$Date,stempsy$Time,sep=" "), format="%d/%m/%Y %T")
  stempsy$WP <- as.numeric(stempsy$Corrected.Water.Potential..MPa.)
  stempsy <- stempsy[!is.na(stempsy$Datetime),]
  head(stempsy)
}

#correct year in datatable$Datetime to stempsy-year when year is not unspecified in slicelist
tmpyearstempsy <- as.integer(format(stempsy$Datetime[100],format="%Y"))
tmpyearslices <- as.integer(format(slicetimestamp$datetime[100],format="%Y"))
if(tmpyearstempsy!=tmpyearslices) {
  slicetimestamp$CharDatetime <- as.character(slicetimestamp$datetime)
  slicetimestamp$datetime <- as.POSIXct(paste(tmpyearstempsy,substr(slicetimestamp$CharDatetime,5,nchar(slicetimestamp$CharDatetime[1])),sep=""))
  tail(slicetimestamp)
}


#####################################################################################################
# if manual WP spot measurements exist: try linear correlation to correct stempsy values
#####################################################################################################

if(exists("manpsy")) { remove(manpsy); }
if(exists("manpsy2")) { remove(manpsy2); }
if(exists("psycorrdata")) { remove(psycorrdata); }
if(exists("minwp")) { remove(minwp); }
if(exists("maxwp")) { remove(maxwp); }
if(exists("stempsycorr")) { remove(stempsycorr); }
psycorrected=0;

manpsy <- read.csv("stempsy-manual.csv",sep=",", stringsAsFactors=FALSE)
manpsy <- manpsy[!is.na(manpsy$WP),]
if(exists("manpsy")) {
  manpsy2 <- manpsy
  manpsy2$Datetime <- as.POSIXct(manpsy2$Datetime, format="%Y-%m-%d %H:%M")
  if(is.na(manpsy2$Datetime[1])) { 
    manpsy2$Datetime <- as.POSIXct(manpsy$Datetime, format="%d/%m/%Y %H:%M")
  }
  manpsy <- manpsy2[!is.na(manpsy2$Datetime),]
  #mn: check the warning here! 
  head(manpsy)
  
  if(exists("stempsy") & nrow(manpsy)>1) {
    psycorrdata <- stempsy[1,c("Datetime","WP")]
    for(i in 1:nrow(manpsy)) {
      psycorrdata[i,] <- stempsy[stempsy$Datetime>=manpsy[i,]$Datetime,][1,c("Datetime","WP")]
    }
    psycorrdata$DatetimeMan <- manpsy$Datetime
    psycorrdata$WPMan <- as.numeric(manpsy$WP)
    psycorrdata <- psycorrdata[!is.na(psycorrdata$WPMan),]
    psycorrdata <- psycorrdata[!is.na(psycorrdata$WP),]
    psycorrdata
    
    minwp <- min(c(psycorrdata$WP, psycorrdata$WPMan))
    maxwp <- max(c(psycorrdata$WP, psycorrdata$WPMan))
    plot(psycorrdata$WP, psycorrdata$WPMan, main="StemPsy vs. Manual", xlim=c(minwp, maxwp), ylim=c(minwp, maxwp), xlab="Stem Psychrometer [MPa]", ylab="Manual Measurement [MPa]")
    psycorr <- lm(psycorrdata$WPMan~psycorrdata$WP)
    psycorr$coefficients[1] #intercept
    psycorr$coefficients[2] #slope
    summary(psycorr)$adj.r.squared #rsquared
    abline(psycorr, col=2)
    lines(c(-10,2),c(-10,2), lty=2)
    
    text(minwp, maxwp, paste(
      " y =", round(psycorr$coefficients[2],3), "x +", round(psycorr$coefficients[1],2),"\n",
      "Rsquared:",round(summary(psycorr)$adj.r.squared,3)
    ), adj=c(0,1), col=2)
    
    tmptimestamp <- format(Sys.time(),"%y%m%d-%H%M%S")
    if(output==1) {
      dev.copy(jpeg,filename=paste(samplename,"-","stempsy-correction-",tmptimestamp,".jpg", sep=""), width=6, height=6, res=300,units="in"); dev.off ();
    }
    
    if(!is.na(psycorr$coefficients[2])) {
      stempsycorr <- stempsy[,c("Datetime","WP")]
      stempsycorr$NewWP <- stempsycorr$WP*psycorr$coefficients[2]+psycorr$coefficients[1]
      head(stempsycorr)
      psycorrected = 1
    }
  } 
  if(!exists("stempsy")){ 
    #if stempsy data doesn't exist, use manual spot measurements only
    stempsy <- manpsy
  }
}


#####################################################################################################
# create datatable with relative and cumulative pixel areas
#####################################################################################################
if(!exists("slicetimestamp")) { slicetimestamp <- data.frame(n=1:nrow(allcombined),timestamp=as.POSIXct(1:nrow(allcombined), format="%M", origin="1970-01-01")) }
datatable <- data.frame(list(Nr=(1:length(slicelist)), Datetime=slicetimestamp$datetime, Filename=as.character(slicelist), Allvessels=allcombined$Total.Area, Allrel=0, Firstorder=firstorder$Total.Area, Firstrel=0, Secondorder=secondorder$Total.Area, Secondrel=0, Thirdorder=thirdorder$Total.Area, Thirdrel=0))
datatable


datatable$Allrel <- datatable$Allvessels/sum(datatable$Allvessels)*100
datatable$Firstrel <- datatable$Firstorder/sum(datatable$Firstorder)*100
datatable$Secondrel <- datatable$Secondorder/sum(datatable$Secondorder)*100
datatable$Thirdrel <- datatable$Thirdorder/sum(datatable$Thirdorder)*100

datatable$Allcumul <- cumsum(datatable$Allvessels/sum(datatable$Allvessels)*100)
datatable$Firstcumul <- cumsum(datatable$Firstorder/sum(datatable$Firstorder)*100)
datatable$Secondcumul <- cumsum(datatable$Secondorder/sum(datatable$Secondorder)*100)
datatable$Thirdcumul <- cumsum(datatable$Thirdorder/sum(datatable$Thirdorder)*100)
head(datatable)


#####################################################################################################
# plot area vs. time
#####################################################################################################
remove(xlimit)
xlimit <- range(na.omit(stempsy$Datetime))
if(!exists("xlimit")) { xlimit <- range(na.omit(datatable$Datetime)) }

if(xlimit[1]>range(datatable$Datetime)[1]) { 
  xlimit[1] = range(datatable$Datetime)[1]
}
if(xlimit[2]<range(datatable$Datetime)[2]) { 
  xlimit[2] = range(datatable$Datetime)[2]
}

#par("mar") <- 5.1 4.1 4.1 2.1 #default values
par("mar"=c(9.1,4.1,4.1,2.1))
plot(datatable$Datetime,datatable$Allcumul, type="l", col=1, xlim=xlimit, ylim=c(0,100), xaxt="n", xlab="", ylab="% cumulative Area", main=samplename, las=1)
par(mgp=c(3,0.5,0)); 
axis.POSIXct(1,datatable$Datetime,format="%d/%m %H:%M")
par(mgp=c(3,1,0))
lines(datatable$Datetime, datatable$Firstcumul, col="#990000")
lines(datatable$Datetime, datatable$Secondcumul, col="#ff0000")
lines(datatable$Datetime, datatable$Thirdcumul, col="#ff9900")
abline(h=50, lty=2, col="#999999")
#text(rep(xlimit[1],4),c(100,95,90,85),c("all combined","1st order","2nd order","3rd order"),col=c(1,"#990000","#ff0000","#ff9900"), adj=0)

AreaAll <- sum(datatable$Allvessels)
AreaFirst <- sum(datatable$Firstorder)
AreaSecond <- sum(datatable$Secondorder)
AreaThird <- sum(datatable$Thirdorder)


#####################################################################################################
# plot area vs. time -- add stempsy data
#####################################################################################################
if(exists("stempsy")) {
  if(psycorrected==0) { stempsydata <- stempsy; ylimit <- c(min(stempsydata$WP),0); }
  if(psycorrected==1) { stempsydata <- stempsycorr; stempsydataUNCORR <- stempsy; colnames(stempsydata) <- c("Datetime","WPold","WP"); ylimit <- c(min(na.omit(c(stempsydata$WP, stempsydata$WPold))),0); }
  
  par(new=TRUE); plot(stempsydata$Datetime, stempsydata$WP, type="l", xlim=xlimit, ylim=ylimit, xaxt="n", yaxt="n", xlab="", ylab="", lty=3, lwd=1, col=4)
  #double-check time-alignment  
  par(mgp=c(3,0.5,0)); 
  axis.POSIXct(1,datatable$Datetime,format="%d/%m %H:%M")
  par(mgp=c(3,1,0))
  axis(side=4, col.axis=4, las=1)
  
  if(psycorrected==1) {
    lines(stempsydata$Datetime,stempsydata$WPold, lty=3, col="#9999ff")
  }
  if(exists("manpsy")) {
    if(exists("psycorrdata")) { 
      points(psycorrdata$DatetimeMan,psycorrdata$WPMan, col=4)
    } else {
      points(manpsy$Datetime,manpsy$WP, col=4)
    }
  }
  
  
  datatable$AllDistFifty <- abs(50-datatable$Allcumul)
  datatable$FirstDistFifty <- abs(50-datatable$Firstcumul)
  datatable$SecondDistFifty <- abs(50-datatable$Secondcumul)
  datatable$ThirdDistFifty <- abs(50-datatable$Thirdcumul)
  
  T50all <- datatable[datatable$AllDistFifty==min(datatable$AllDistFifty),]$Datetime[1]
  T50first <- datatable[datatable$FirstDistFifty==min(datatable$FirstDistFifty),]$Datetime[1]
  T50second <- datatable[datatable$SecondDistFifty==min(datatable$SecondDistFifty),]$Datetime[1]
  T50third <- datatable[datatable$ThirdDistFifty==min(datatable$ThirdDistFifty),]$Datetime[1]
  
  T50all
  T50first
  T50second
  T50third
  
  
  
  P50allpre <- stempsydata[stempsydata$Datetime<=T50all,]
  P50allpre <- P50allpre[nrow(P50allpre),]
  P50allpost <- stempsydata[stempsydata$Datetime>=T50all,][1,]
  
  if(abs(P50allpost$WP-P50allpre$WP)>0.06) {
    P50all <- paste("[",round(P50allpre$WP,2),", ",round(P50allpost$WP,2), "]", sep="")
  } else { 
    P50all <- round(P50allpost$WP,2)
  }
  
  P50firstpre <- stempsydata[stempsydata$Datetime<=T50first,]
  P50firstpre <- P50firstpre[nrow(P50firstpre),]
  P50firstpost <- stempsydata[stempsydata$Datetime>=T50first,][1,]
  
  if(abs(P50firstpost$WP-P50firstpre$WP)>0.06) {
    P50first <- paste("[",round(P50firstpre$WP,2),", ",round(P50firstpost$WP,2), "]", sep="")
  } else { 
    P50first <- round(P50firstpost$WP,2)
  }
  P50secondpre <- stempsydata[stempsydata$Datetime<=T50second,]
  P50secondpre <- P50secondpre[nrow(P50secondpre),]
  P50secondpost <- stempsydata[stempsydata$Datetime>=T50second,][1,]
  
  if(abs(P50secondpost$WP-P50secondpre$WP)>0.06) {
    P50second <- paste("[",round(P50secondpre$WP,2),", ",round(P50secondpost$WP,2), "]", sep="")
  } else { 
    P50second <- round(P50secondpost$WP,2)
  }
  
  P50thirdpre <- stempsydata[stempsydata$Datetime<=T50third,]
  P50thirdpre <- P50thirdpre[nrow(P50thirdpre),]
  P50thirdpost <- stempsydata[stempsydata$Datetime>=T50third,][1,]
  
  if(abs(P50thirdpost$WP-P50thirdpre$WP)>0.06) {
    P50third <- paste("[",round(P50thirdpre$WP,2),", ",round(P50thirdpost$WP,2), "]", sep="")
  } else { 
    P50third <- round(P50thirdpost$WP,2)
  }
  
  
  
  P50all
  P50first
  P50second
  P50third
  
  
  
  if(psycorrected==1) {
    
    P50allUNCORRpre <- stempsydataUNCORR[stempsydataUNCORR$Datetime<=T50all,]
    P50allUNCORRpre <- P50allUNCORRpre[nrow(P50allUNCORRpre),]
    P50allUNCORRpost <- stempsydataUNCORR[stempsydataUNCORR$Datetime>=T50all,][1,]
    
    if(abs(P50allUNCORRpost$WP-P50allUNCORRpre$WP)>0.06) {
      P50allUNCORR <- paste("[",round(P50allUNCORRpre$WP,2),", ",round(P50allUNCORRpost$WP,2), "]", sep="")
    } else { 
      P50allUNCORR <- round(P50allUNCORRpost$WP,2)
    }
    
    P50firstUNCORRpre <- stempsydataUNCORR[stempsydataUNCORR$Datetime<=T50first,]
    P50firstUNCORRpre <- P50firstUNCORRpre[nrow(P50firstUNCORRpre),]
    P50firstUNCORRpost <- stempsydataUNCORR[stempsydataUNCORR$Datetime>=T50first,][1,]
    
    if(abs(P50firstUNCORRpost$WP-P50firstUNCORRpre$WP)>0.06) {
      P50firstUNCORR <- paste("[",round(P50firstUNCORRpre$WP,2),", ",round(P50firstUNCORRpost$WP,2), "]", sep="")
    } else { 
      P50firstUNCORR <- round(P50firstUNCORRpost$WP,2)
    }
    P50secondUNCORRpre <- stempsydataUNCORR[stempsydataUNCORR$Datetime<=T50second,]
    P50secondUNCORRpre <- P50secondUNCORRpre[nrow(P50secondUNCORRpre),]
    P50secondUNCORRpost <- stempsydataUNCORR[stempsydataUNCORR$Datetime>=T50second,][1,]
    
    if(abs(P50secondUNCORRpost$WP-P50secondUNCORRpre$WP)>0.06) {
      P50secondUNCORR <- paste("[",round(P50secondUNCORRpre$WP,2),", ",round(P50secondUNCORRpost$WP,2), "]", sep="")
    } else { 
      P50secondUNCORR <- round(P50secondUNCORRpost$WP,2)
    }
    
    P50thirdUNCORRpre <- stempsydataUNCORR[stempsydataUNCORR$Datetime<=T50third,]
    P50thirdUNCORRpre <- P50thirdUNCORRpre[nrow(P50thirdUNCORRpre),]
    P50thirdUNCORRpost <- stempsydataUNCORR[stempsydataUNCORR$Datetime>=T50third,][1,]
    
    if(abs(P50thirdUNCORRpost$WP-P50thirdUNCORRpre$WP)>0.06) {
      P50thirdUNCORR <- paste("[",round(P50thirdUNCORRpre$WP,2),", ",round(P50thirdUNCORRpost$WP,2), "]", sep="")
    } else { 
      P50thirdUNCORR <- round(P50thirdUNCORRpost$WP,2)
    }
    
    
  }
  
}

#####################################################################################################
# plot area vs. time -- add text to plot
#####################################################################################################

par(new=TRUE); plot(NA,NA, xlim=xlimit, ylim=c(0,100), xaxt="n", xlab="", yaxt="n", ylab="")  
par("xpd"=NA)
if(exists("stempsy")) { 
  if(psycorrected==0) {
    text(xlimit[1],-20,paste("Leaf P50 (not curve-fitted)"), col=1, adj=0)
    text(xlimit[1],-27,paste("entire leaf:",P50all,"MPa"), col=1, adj=0)
    text(xlimit[1],-34,paste("1st order:",P50first,"MPa"), col="#990000", adj=0)
    text(xlimit[1],-41,paste("2nd order:",P50second,"MPa"), col="#ff0000", adj=0)
    text(xlimit[1],-48,paste("3rd order:",P50third,"MPa"), col="#ff9900", adj=0)
    #text(rep(xlimit[1],4),c(-20,-27,-34,-41,-48),c(paste("Leaf P50 (not curve-fitted)"), paste("entire leaf:",P50all,"MPa"),paste("1st order:",P50first,"MPa"),paste("2nd order:",P50second,"MPa"),paste("3rd order:",P50third,"MPa")),col=c(1,1,"#990000","#ff0000","#ff9900"), adj=0)
  }
  if(psycorrected==1) {
    text(xlimit[1],-20,paste("Leaf P50 (not curve-fitted)","(corrected | uncorrected WP)"),col=1, adj=0)
    text(xlimit[1],-27,paste("entire leaf:",P50all,"MPa |",P50allUNCORR,"MPa"),col=1, adj=0)
    text(xlimit[1],-34,paste("1st order:",P50first,"MPa |",P50firstUNCORR,"MPa"),col="#990000", adj=0)
    text(xlimit[1],-41,paste("2nd order:",P50second,"MPa |",P50secondUNCORR,"MPa"),col="#ff0000", adj=0)
    text(xlimit[1],-48,paste("3rd order:",P50third,"MPa |",P50thirdUNCORR,"MPa"),col="#ff9900", adj=0)
    #text(rep(xlimit[1],4),c(-20,-27,-34,-41,-48),c(paste("Leaf P50 (not curve-fitted)","(corrected | uncorrected WP)"), paste("entire leaf:",P50all,"MPa |",P50allUNCORR,"MPa"),paste("1st order:",P50first,"MPa |",P50firstUNCORR,"MPa"),paste("2nd order:",P50second,"MPa |",P50secondUNCORR,"MPa"),paste("3rd order:",P50third,"MPa |",P50thirdUNCORR,"MPa")),col=c(1,1,"#990000","#ff0000","#ff9900"), adj=0)
  }
} else { 
  text(xlimit[1],-20,"stempsy data N/A", adj=0)
}

text(xlimit[2],-20,"Total Area",col=1, adj=1)
text(xlimit[2],-27,paste("entire leaf:",AreaAll,"px^2"), col=1, adj=1)
text(xlimit[2],-34,paste("1st order:",AreaFirst,"px^2"), col="#990000", adj=1)
text(xlimit[2],-41,paste("2nd order:",AreaSecond,"px^2"),col="#ff0000", adj=1)
text(xlimit[2],-48,paste("3rd order:",AreaThird,"px^2"), col="#ff9900", adj=1)
#text(rep(xlimit[2],4),c(-20,-27,-34,-41,-48),c("Total Area",paste("entire leaf:",AreaAll,"px^2"),paste("1st order:",AreaFirst,"px^2"),paste("2nd order:",AreaSecond,"px^2"),paste("3rd order:",AreaThird,"px^2")),col=c(1,1,"#990000","#ff0000","#ff9900"), adj=1)


#if(!exists("stempsy")) {
# text(xlimit[1],100,"stempsy data N/A", adj=0)
# 
#}
par("xpd"=FALSE)




tmptimestamp <- format(Sys.time(),"%y%m%d-%H%M%S")
if(output==1) {
  dev.copy(jpeg,filename=paste(samplename,"-","LeafVC-",tmptimestamp,"-1-unfitted.jpg", sep=""), width=8, height=6, res=300,units="in"); dev.off ();
}





#####################################################################################################
# transform data to WP vs. area (rather than time vs. area)
#####################################################################################################
fitdata <- stempsydata
head(fitdata)
head(datatable)

fitdata$Allcumul=0
fitdata$Firstcumul=0
fitdata$Secondcumul=0
fitdata$Thirdcumul=0
for(fx in 1:nrow(fitdata)){
  searchtime <- fitdata[fx,]$Datetime
  tmpline <- datatable[datatable$Datetime<=searchtime,]
  tmpline <- tmpline[nrow(tmpline),] 
  if(nrow(tmpline)>0) {
    fitdata[fx,"Allcumul"] <- tmpline$Allcumul
    fitdata[fx,"Firstcumul"] <- tmpline$Firstcumul
    fitdata[fx,"Secondcumul"] <- tmpline$Secondcumul
    fitdata[fx,"Thirdcumul"] <- tmpline$Thirdcumul
  }
}
tail(fitdata)


if(output==1) { write.csv(fitdata,"output-wp-vs-area.csv") }

#####################################################################################################
# fit P50 values rather than just look at when the cumulative sum exceeds 50%
#####################################################################################################

#function(fitdata) {
#####################################################################################################
# remove WP data that rises again (more than above 0.3 MPa) after reaching the minimum
#####################################################################################################
#if(output==1) { fitdata <- read.csv("output-wp-vs-area.csv") }
rownames(fitdata) <- 1:nrow(fitdata)
tmpminwp <- min(fitdata$WP)
fitdata$tmpuse = 1
tmpminrow = min(rownames(fitdata[fitdata$WP==tmpminwp,]))
#fitdata[(as.integer(tmpminrow)+1):nrow(fitdata),]$tmpuse=0
tmpfitdata <- fitdata[(as.integer(tmpminrow)+1):nrow(fitdata),]
(tmpcutoffrow <- min(as.integer(rownames(tmpfitdata[round(tmpfitdata$WP,1)>(round(tmpminwp+0.05,1)),]))) )
fitdata[(as.integer(tmpcutoffrow)):nrow(fitdata),]$tmpuse=0

print(paste("removing rows ",tmpcutoffrow," to ",nrow(fitdata)," (min of ",round(tmpminwp,2)," MPa reached at row ",tmpminrow," = ",fitdata[tmpminrow,"Datetime"],").",sep=""))
#debug 
#tail(fitdata)
#fitdata[1:130,c(1,2,3,4,5,9)]
#fitdata[120:220,c(1,2,3,4,5,9)]

croppedfitdata <- fitdata[fitdata$tmpuse==1,]

plot(-croppedfitdata$WP, croppedfitdata$Allcumul, ylim=c(0,100), ylab="Percent cumul. area (%)", xlab="Water potential (-MPa)", col=1)
par(new=TRUE); plot(-croppedfitdata$WP, croppedfitdata$Firstcumul, ylim=c(0,100), xaxt="n", yaxt="n", ylab="", xlab="", col="#990000")
par(new=TRUE); plot(-croppedfitdata$WP, croppedfitdata$Secondcumul, ylim=c(0,100), xaxt="n", yaxt="n", ylab="", xlab="", col="#ff0000")
par(new=TRUE); plot(-croppedfitdata$WP, croppedfitdata$Thirdcumul, ylim=c(0,100), xaxt="n", yaxt="n", ylab="", xlab="", col="#ff9900")


if(exists("fitAll")) { remove(fitAll); }
if(exists("fitFirst")) { remove(fitFirst); }
if(exists("fitSecond")) { remove(fitSecond); }
if(exists("fitThird")) { remove(fitThird); }

(fitAll <- fitplc(croppedfitdata,varnames=c(PLC="Allcumul", WP="WP")))
(fitFirst <- fitplc(croppedfitdata,varnames=c(PLC="Firstcumul", WP="WP")))
(fitSecond <- fitplc(croppedfitdata,varnames=c(PLC="Secondcumul", WP="WP")))
(fitThird <- fitplc(croppedfitdata,varnames=c(PLC="Thirdcumul", WP="WP")))


#plot individually to scroll back plot(-croppedfitdata$WP, croppedfitdata$Allcumul, ylim=c(0,100), ylab="Percent cumul. area (%)", xlab="Water potential (-MPa)", col=1)
plot(fitAll, what="embol", main=samplename, xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), linecol="#000000", pointcol="#000000", plotPx=FALSE, plotci=FALSE, pch=1, ylab="Percent cumul. area (%)", xlab="")
plot(fitFirst, what="embol", main=samplename, xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), linecol="#990000", pointcol="#990000", plotPx=FALSE, plotci=FALSE, pch=1, ylab="Percent cumul. area (%)", xlab="")
plot(fitSecond, what="embol", main=samplename, xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), linecol="#ff0000", pointcol="#ff0000", plotPx=FALSE, plotci=FALSE, pch=1, ylab="Percent cumul. area (%)", xlab="")
plot(fitThird, what="embol", main=samplename, xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), linecol="#ff9900", pointcol="#ff9900", plotPx=FALSE, plotci=FALSE, pch=1, ylab="Percent cumul. area (%)", xlab="")

plot(fitAll, what="embol", main=samplename, xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), linecol="#000000", plotPx=FALSE, plotci=FALSE, pch=1, ylab="Percent cumul. area (%)", xlab="")
if(exists("fitFirst")) { par(new=TRUE); plot(fitFirst, what="embol", xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), pointcol="#990000",linecol="#990000", plotPx=FALSE, plotci=FALSE, pch=1, xlab="", ylab="") }
if(exists("fitSecond")) { par(new=TRUE); plot(fitSecond, what="embol", xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), pointcol="#ff0000", linecol="#ff0000", plotPx=FALSE, plotci=FALSE, pch=1, xlab="", ylab="") }
if(exists("fitThird")) { par(new=TRUE); plot(fitThird, what="embol", xlim=c(0,-min(croppedfitdata$WP)), ylim=c(0,100), pointcol="#ff9900",  linecol="#ff9900", plotPx=FALSE, plotci=FALSE, pch=1, xlab="", ylab="") } 

par("xpd"=FALSE)
abline(h=50, col=1, lty=2, lwd=1)
par("xpd"=NA)

par(new=TRUE); plot(NA,NA, xlim=xlimit, ylim=c(0,100), xaxt="n", xlab="", yaxt="n", ylab="")  
par("xpd"=NA)

text(xlimit[1],-20,paste("Leaf P50 (curve-fitted)"),col=1, adj=0)
text(xlimit[1],-27,paste("entire leaf: -",round(fitAll$cipars[2,1],2)," MPa", sep=""), col=1, adj=0)
text(xlimit[1],-34,paste("1st order: -",round(fitFirst$cipars[2,1],2)," MPa", sep=""), col="#990000", adj=0) 
text(xlimit[1],-41,paste("2nd order: -",round(fitSecond$cipars[2,1],2)," MPa", sep=""), col="#ff0000", adj=0)
text(xlimit[1],-48,paste("3rd order: -",round(fitThird$cipars[2,1],2)," MPa", sep=""),col="#ff9900", adj=0)
#text(rep(xlimit[1],5),c(-20,-27,-34,-41,-48),c(paste("Leaf P50 (curve-fitted)"), paste("entire leaf:",round(fitAll$cipars[2,1],2),"MPa"),paste("1st order:",round(fitFirst$cipars[2,1],2),"MPa"),paste("2nd order:",round(fitSecond$cipars[2,1],2),"MPa"),paste("3rd order:",round(fitThird$cipars[2,1],2),"MPa")),col=c(1,1,"#990000","#ff0000","#ff9900"), adj=0)

if(psycorrected==1) { text(mean(xlimit),-20, "WP corrected!", adj=0.5) }
#if(psycorrected==1) { text(mean(xlimit),-20, "(WP)", adj=0.5) }
text(xlimit[2],-20,"Total Area",col=1, adj=1)
text(xlimit[2],-27,paste("entire leaf:",AreaAll,"px^2"),col=1, adj=1)
text(xlimit[2],-34,paste("1st order:",AreaFirst,"px^2"),col="#990000", adj=1)
text(xlimit[2],-41,paste("2nd order:",AreaSecond,"px^2"),col="#ff0000", adj=1)
text(xlimit[2],-48,paste("3rd order:",AreaThird,"px^2"),col="#ff9900", adj=1)
#text(rep(xlimit[2],4),c(-20,-27,-34,-41,-48),c("Total Area",paste("entire leaf:",AreaAll,"px^2"),paste("1st order:",AreaFirst,"px^2"),paste("2nd order:",AreaSecond,"px^2"),paste("3rd order:",AreaThird,"px^2")),col=c(1,1,"#990000","#ff0000","#ff9900"), adj=1)

par("xpd"=FALSE)

if(output==1) {
  dev.copy(jpeg,filename=paste(samplename,"-","LeafVC-",tmptimestamp,"-2-weibull.jpg", sep=""), width=8, height=6, res=300,units="in"); dev.off ();
}

fitAll
fitFirst
fitSecond
fitThird

#  return(list(fitdata=fitdata,fitAll=fitAll,fitFirst=fitFirst,fitSecond=fitSecond,fitThird=fitThird))
#}


#####################################################################################################
# Prepare WP-binning for Temporal Color Coding in ImageJ
#####################################################################################################

#fitdata <- read.csv("output-wp-vs-area.csv")
tmpinterval = 0.5 #specify interval in MPa

head(fitdata)
fitdata$WPround <- round(fitdata$WP,1)
head(fitdata)

fitdata$bin = 0
fitdata[1,]$bin = 1; tmpstep = -floor((abs(fitdata[1,]$WPround)*2)-0.01)/2; tmpbin = 1;
#for(i in 2:205) { 
for(i in 2:nrow(fitdata)) {
  if(abs(abs(fitdata[i,]$WPround)-abs(tmpstep))<tmpinterval) {
    #if((fitdata[i,]$WPround-tmpstep)<tmpinterval & (fitdata[i,]$WPround-tmpstep)>-tmpinterval) {
    fitdata[i,]$bin = tmpbin; 
  } else {
    if(fitdata[i,]$WPround>(tmpstep)) { fitdata[i,]$bin=tmpbin; } else {
      tmpbin = tmpbin+1;
      tmpstep = tmpstep-tmpinterval;
      fitdata[i,]$bin = tmpbin;
    }
  }
}
head(fitdata,15)
#fitdata
#fitdata[186:210,]
#fitdata[210:410,]
#tail(fitdata, 100)


nbins = max(fitdata$bin)
zstacktable <- data.frame(list(WPupper=rep(0,nbins),WPlower=rep(-0.5,nbins),DateTimeStart=rep(fitdata[1,"Datetime"],nbins),DateTimeEnd=rep(fitdata[1,"Datetime"],nbins),SliceStart=rep(1,nbins),SliceEnd=rep(1,nbins),FileStart=rep("abc",nbins),FileEnd=rep("abc",nbins)), stringsAsFactors=FALSE)
for(i in min(fitdata$bin):max(fitdata$bin)) {
  zstacktable[i,1:2] = c(-(i-1)*tmpinterval,-(i)*tmpinterval);
  tmpdatetime <- fitdata[fitdata$bin==i,]$Datetime
  zstacktable[i,3] = min(tmpdatetime)
  zstacktable[i,4] = max(tmpdatetime)
  tmpdatatable <- datatable[(datatable$Datetime>=min(tmpdatetime) & datatable$Datetime<max(tmpdatetime)),]
  if(nrow(tmpdatatable)<1) { tmpdatatable <- datatable[(datatable$Datetime>=min(tmpdatetime) & datatable$Datetime<=max(tmpdatetime)),] }
  if(nrow(tmpdatatable)<1) { tmpdatatable[1,] <- rep(NA,19) }
  zstacktable[i,"SliceStart"] = tmpdatatable[1,1]
  zstacktable[i,"SliceEnd"] = tmpdatatable[nrow(tmpdatatable),1]
  zstacktable[i,"FileStart"] = as.character(tmpdatatable[1,3])
  zstacktable[i,"FileEnd"] = as.character(tmpdatatable[nrow(tmpdatatable),3])
}
zstacktable[nbins,2] <- NA
zstacktable

if(output==1) {
  write.csv(zstacktable,"output-slices-binned-by-wp.csv")
}

